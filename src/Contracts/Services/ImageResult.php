<?php

namespace Smorken\LenelImage\Contracts\Services;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Http\Response;
use Smorken\Service\Contracts\Services\VO\VOResult;

/**
 * @property string|int $identifier
 * @property string|int $requesterId
 * @property ?\Smorken\Image\Sizer\Contracts\Result $result
 *
 * @phpstan-require-extends \Smorken\LenelImage\Services\ImageResult
 */
interface ImageResult extends VOResult
{
    public function getEventDispatcher(): ?Dispatcher;

    public function getHeaders(bool $download = false): array;

    public function getImageExtension(): string;

    public function getImageName(): string;

    public function getImageSize(): int;

    public function getMime(): string;

    public function toResponse(bool $download = true): Response;

    public static function setEventDispatcher(Dispatcher $dispatcher): void;

    public static function setHeadersHelperClass(string $headersHelperClass): void;
}
