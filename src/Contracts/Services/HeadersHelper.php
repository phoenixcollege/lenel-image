<?php

namespace Smorken\LenelImage\Contracts\Services;

interface HeadersHelper
{
    public function getHeaders(bool $download = false): array;

    public function getImageResult(): ImageResult;

    public function getExpires(): string;
}
