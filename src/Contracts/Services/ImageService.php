<?php

namespace Smorken\LenelImage\Contracts\Services;

use Illuminate\Http\Request;
use Smorken\Image\Sizer\Contracts\Sizer;
use Smorken\LenelImage\Contracts\Storage\Lenel;
use Smorken\Service\Contracts\Services\BaseService;

interface ImageService extends BaseService
{
    public function findByAlternateId(Request $request, int|string $alternateId): ImageResult;

    public function findByStudentId(Request $request, int|string $studentId): ImageResult;

    public function findByStudentIdAlternateId(
        Request $request,
        int|string $studentId,
        int|string $alternateId
    ): ImageResult;

    public function getProvider(): Lenel;

    public function getSizer(): Sizer;

    public function getSizerOptions(): array;

    public function setEncode(bool $encode): void;

    public function setSizerOption(string $key, mixed $value): void;

    public function setSizerOptions(array $options): void;
}
