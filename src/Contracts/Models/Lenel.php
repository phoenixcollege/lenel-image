<?php

namespace Smorken\LenelImage\Contracts\Models;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Model\Contracts\Model;

/**
 * @property mixed $LNL_BLOB
 *
 * @method Builder studentIdIs(int|string $studentId)
 * @method Builder alternateIdIs(int|string $alternateId, bool $or = false)
 *
 * @phpstan-require-extends \Smorken\LenelImage\Models\Eloquent\Lenel
 */
interface Lenel extends Model
{
    public function findBy(int|string $studentId, int|string $alternateId): ?Image;

    public function findByAlternateId(int|string $alternateId): ?Image;

    public function findByStudentId(int|string $studentId): ?Image;

    public static function setIdKeys(array $idKeys): void;
}
