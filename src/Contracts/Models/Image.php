<?php

namespace Smorken\LenelImage\Contracts\Models;

/**
 * @property string $mime
 * @property string $data
 * @property int $size
 *
 * @phpstan-require-extends \Smorken\LenelImage\Models\Image
 */
interface Image {}
