<?php

namespace Smorken\LenelImage\Contracts\Storage;

use Smorken\LenelImage\Contracts\Models\Image;

interface Lenel
{
    public function findByAlternateId(string|int $alternateId): ?Image;

    public function findByStudentId(string|int $studentId): ?Image;

    public function findByStudentIdAlternateId(string|int $studentId, string|int $alternateId): ?Image;

    public function getModel(): \Smorken\LenelImage\Contracts\Models\Lenel;
}
