<?php

namespace Smorken\LenelImage\Contracts\Enums;

interface ImageIdKeys
{
    public const STUDENT_ID = 'student_id';

    public const ALTERNATE_ID = 'alternate_id';
}
