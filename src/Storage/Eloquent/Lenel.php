<?php

namespace Smorken\LenelImage\Storage\Eloquent;

use Smorken\LenelImage\Contracts\Models\Image;

class Lenel implements \Smorken\LenelImage\Contracts\Storage\Lenel
{
    public function __construct(protected \Smorken\LenelImage\Contracts\Models\Lenel $model) {}

    public function findByAlternateId(int|string $alternateId): ?Image
    {
        return $this->getModel()->findByAlternateId($alternateId);
    }

    public function findByStudentId(int|string $studentId): ?Image
    {
        return $this->getModel()->findByStudentId($studentId);
    }

    public function findByStudentIdAlternateId(int|string $studentId, int|string $alternateId): ?Image
    {
        return $this->getModel()->findBy($studentId, $alternateId);
    }

    public function getModel(): \Smorken\LenelImage\Contracts\Models\Lenel
    {
        return $this->model;
    }
}
