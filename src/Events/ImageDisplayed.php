<?php

namespace Smorken\LenelImage\Events;

use Carbon\Carbon;
use Smorken\LenelImage\Enums\DisplayType;

class ImageDisplayed
{
    public Carbon $date;

    public function __construct(
        public string|int $identifier,
        public string|int $requesterId,
        public DisplayType $displayType
    ) {
        $this->date = Carbon::now();
    }
}
