<?php

namespace Smorken\LenelImage;

use Illuminate\Contracts\Events\Dispatcher;
use Smorken\Image\Sizer\Contracts\Sizer;
use Smorken\LenelImage\Contracts\Enums\ImageIdKeys;
use Smorken\LenelImage\Contracts\Services\ImageService;
use Smorken\LenelImage\Contracts\Storage\Lenel;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Bootstrap the application events.
     */
    public function boot(): void
    {
        $this->bootConfig();
        $this->setImageDisplayKeysOnModel();
    }

    /**
     * Register the service provider.
     */
    public function register(): void
    {
        $this->bindStorage();
        $this->bindService();
    }

    protected function bindService(): void
    {
        $this->app->bind(ImageService::class, fn ($app) => new \Smorken\LenelImage\Services\ImageService(
            $app[Lenel::class],
            $app[Sizer::class],
            $app[Dispatcher::class],
            $app['config']->get('lenelimage.image', [])
        ));
    }

    protected function bindStorage(): void
    {
        $this->app->bind(Lenel::class, function ($app) {
            $storageClass = $app['config']->get('lenelimage.storage.class',
                \Smorken\LenelImage\Storage\Eloquent\Lenel::class);
            $modelClass = $app['config']->get('lenelimage.model.class',
                \Smorken\LenelImage\Models\Eloquent\Lenel::class);

            return new $storageClass(new $modelClass);
        });
    }

    protected function bootConfig(): void
    {
        $config = __DIR__.'/../config/config.php';
        $this->mergeConfigFrom($config, 'lenelimage');
        // @phpstan-ignore function.notFound
        $this->publishes([$config => config_path('lenelimage.php')], 'config');
    }

    protected function setImageDisplayKeysOnModel(): void
    {
        $studentIdKey = $this->app['config']->get('lenelimage.model.student_id_key', 'SSNO');
        $alternateIdKey = $this->app['config']->get('lenelimage.model.alternate_id_key', 'OPHONE');
        $modelClass = $this->app['config']->get('lenelimage.model.class',
            \Smorken\LenelImage\Models\Eloquent\Lenel::class);
        $modelClass::setIdKeys([
            ImageIdKeys::ALTERNATE_ID => $alternateIdKey,
            ImageIdKeys::STUDENT_ID => $studentIdKey,
        ]);
    }
}
