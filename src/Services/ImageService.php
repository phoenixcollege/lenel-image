<?php

namespace Smorken\LenelImage\Services;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Http\Request;
use Smorken\Image\Sizer\Contracts\Result;
use Smorken\Image\Sizer\Contracts\Sizer;
use Smorken\LenelImage\Contracts\Models\Image;
use Smorken\LenelImage\Contracts\Services\ImageResult;
use Smorken\LenelImage\Contracts\Storage\Lenel;
use Smorken\Service\Services\BaseService;

class ImageService extends BaseService implements \Smorken\LenelImage\Contracts\Services\ImageService
{
    protected array $sizerOptions = [
        'base64' => false,
    ];

    protected string $voClass = \Smorken\LenelImage\Services\ImageResult::class;

    public function __construct(
        protected Lenel $provider,
        protected Sizer $sizer,
        ?Dispatcher $dispatcher,
        array $sizerOptions = [],
        array $services = []
    ) {
        parent::__construct($services);
        $this->setSizerOptions($sizerOptions);
        if ($dispatcher) {
            $this->setEventDispatcherOnResult($dispatcher);
        }
    }

    public function findByAlternateId(Request $request, int|string $alternateId): ImageResult
    {
        $result = $this->createResult($request, [$alternateId]);
        $image = $this->getProvider()->findByAlternateId($alternateId);

        return $this->resultFromImage($result, $image);
    }

    public function findByStudentId(Request $request, int|string $studentId): ImageResult
    {
        $result = $this->createResult($request, [$studentId]);
        $image = $this->getProvider()->findByStudentId($studentId);

        return $this->resultFromImage($result, $image);
    }

    public function findByStudentIdAlternateId(
        Request $request,
        int|string $studentId,
        int|string $alternateId
    ): ImageResult {
        $result = $this->createResult($request, [$studentId ?: null, $alternateId ?: null]);
        $image = $this->getProvider()->findByStudentIdAlternateId($studentId, $alternateId);

        return $this->resultFromImage($result, $image);
    }

    public function getProvider(): Lenel
    {
        return $this->provider;
    }

    public function getSizer(): Sizer
    {
        return $this->sizer;
    }

    public function getSizerOptions(): array
    {
        return $this->sizerOptions;
    }

    public function setSizerOptions(array $options): void
    {
        $this->sizerOptions = $options;
    }

    public function setEncode(bool $encode): void
    {
        $this->setSizerOption('base64', $encode);
    }

    public function setSizerOption(string $key, mixed $value): void
    {
        $this->sizerOptions[$key] = $value;
    }

    protected function createResult(Request $request, array $identifier): ImageResult
    {
        /** @var \Smorken\LenelImage\Contracts\Services\ImageResult */
        return $this->newVO([
            'identifier' => implode(':', array_filter($identifier)),
            'requesterId' => $this->getUserId($request),
        ]);
    }

    protected function getDefaultImage(ImageResult $result): ?Image
    {
        return null;
    }

    protected function getUserId(Request $request): string|int
    {
        return $request->user()?->getAuthIdentifier() ?? 0;
    }

    protected function resultFromImage(ImageResult $result, ?Image $image): ImageResult
    {
        if (! $image) {
            $image = $this->getDefaultImage($result);
        }
        $result->result = $image ? $this->sizeImage($image) : null;

        return $result;
    }

    protected function setEventDispatcherOnResult(Dispatcher $dispatcher): void
    {
        $this->voClass::setEventDispatcher($dispatcher);
    }

    protected function sizeImage(Image $image): ?Result
    {
        return $this->getSizer()->size($image->data, $this->getSizerOptions());
    }
}
