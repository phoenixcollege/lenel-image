<?php

namespace Smorken\LenelImage\Services;

class HeadersHelper implements \Smorken\LenelImage\Contracts\Services\HeadersHelper
{
    public int $cacheAge = 86400;

    public function __construct(
        protected \Smorken\LenelImage\Contracts\Services\ImageResult $imageResult
    ) {}

    public function getExpires(): string
    {
        return gmdate('D, d M Y H:i:s \G\M\T', time() + $this->cacheAge);
    }

    public function getHeaders(bool $download = false): array
    {
        $headers = $this->getHeadersBase($download);
        if ($download) {
            return $this->getHeadersForDownload($headers);
        }

        return $this->getHeadersForView($headers);
    }

    public function getImageResult(): \Smorken\LenelImage\Contracts\Services\ImageResult
    {
        return $this->imageResult;
    }

    protected function getHeadersBase(bool $download): array
    {
        return [
            'Content-Type' => $this->getImageResult()->getMime(),
            'Content-Transfer-Encoding' => 'binary',
            'Content-Disposition' => sprintf('%s; filename="%s"', $download ? 'attachment' : 'inline',
                $this->getImageResult()->getImageName()),
        ];
    }

    protected function getHeadersForDownload(array $headers): array
    {
        return array_merge($headers, [
            'Content-Length' => $this->getImageResult()->getImageSize(),
            'Cache-Control' => 'must-revalidate',
            'Expires' => '0',
            'Pragma' => 'public',
        ]);
    }

    protected function getHeadersForView(array $headers): array
    {
        return array_merge($headers, [
            'Expires' => $this->getExpires(),
            'Cache-Control' => 'max-age='.$this->cacheAge.', public',
            'Pragma' => 'public',
        ]);
    }
}
