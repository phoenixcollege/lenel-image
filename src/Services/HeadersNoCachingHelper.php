<?php

namespace Smorken\LenelImage\Services;

class HeadersNoCachingHelper implements \Smorken\LenelImage\Contracts\Services\HeadersHelper
{
    public function __construct(
        protected \Smorken\LenelImage\Contracts\Services\ImageResult $imageResult
    ) {}

    public function getExpires(): string
    {
        return '0';
    }

    public function getHeaders(bool $download = false): array
    {
        $headers = $this->getHeadersBase($download);
        if ($download) {
            return $this->getHeadersForDownload($headers);
        }

        return $this->getHeadersForView($headers);
    }

    public function getImageResult(): \Smorken\LenelImage\Contracts\Services\ImageResult
    {
        return $this->imageResult;
    }

    protected function getHeadersBase(bool $download): array
    {
        return [
            'Content-Type' => $this->getImageResult()->getMime(),
            'Content-Transfer-Encoding' => 'binary',
            'Content-Disposition' => sprintf('%s; filename="%s"', $download ? 'attachment' : 'inline',
                $this->getImageResult()->getImageName()),
        ];
    }

    protected function getHeadersForDownload(array $headers): array
    {
        return array_merge($headers, [
            'Content-Length' => $this->getImageResult()->getImageSize(),
            'Cache-Control' => 'must-revalidate',
            'Expires' => '0',
            'Pragma' => 'public',
        ]);
    }

    protected function getHeadersForView(array $headers): array
    {
        return array_merge($headers, [
            'Expires' => $this->getExpires(),
            'Cache-Control' => 'must-revalidate',
            'Pragma' => 'public',
        ]);
    }
}
