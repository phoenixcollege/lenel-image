<?php

namespace Smorken\LenelImage\Services;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Smorken\Image\Sizer\Contracts\Result;
use Smorken\LenelImage\Enums\DisplayType;
use Smorken\LenelImage\Events\ImageDisplayed;
use Smorken\Service\Services\VO\VOResult;

class ImageResult extends VOResult implements \Smorken\LenelImage\Contracts\Services\ImageResult
{
    protected static ?Dispatcher $dispatcher = null;

    protected static string $headersHelperClass = HeadersHelper::class;

    public ?Result $result = null;

    public function __construct(
        public string|int $identifier,
        public string|int $requesterId
    ) {}

    public static function setEventDispatcher(Dispatcher $dispatcher): void
    {
        self::$dispatcher = $dispatcher;
    }

    public static function setHeadersHelperClass(string $headersHelperClass): void
    {
        self::$headersHelperClass = $headersHelperClass;
    }

    public function getEventDispatcher(): ?Dispatcher
    {
        return self::$dispatcher;
    }

    public function getHeaders(bool $download = false): array
    {
        return $this->getHeadersHelper()->getHeaders($download);
    }

    public function getImageExtension(): string
    {
        $mime = $this->getMime();
        if (str_ends_with($mime, 'jpg')) {
            return 'jpg';
        }

        return 'png';
    }

    public function getImageName(): string
    {
        return sprintf('photo-%s.%s', Str::slug($this->identifier), $this->getImageExtension());
    }

    public function getImageSize(): int
    {
        return $this->result?->size ?? 0;
    }

    public function getMime(): string
    {
        return $this->result?->mime ?? 'image/png';
    }

    public function toResponse(bool $download = true): Response
    {
        $this->dispatchEvent($download);
        $headers = $this->getHeaders($download);

        return \Illuminate\Support\Facades\Response::make(
            $this->result?->data,
            $this->result ? 200 : 400,
            $headers
        );
    }

    protected function dispatchEvent(bool $download): void
    {
        $this->getEventDispatcher()?->dispatch(new ImageDisplayed(
            $this->identifier,
            $this->requesterId,
            $download ? DisplayType::DOWNLOAD : DisplayType::VIEW)
        );
    }

    protected function getHeadersHelper(): \Smorken\LenelImage\Contracts\Services\HeadersHelper
    {
        return new self::$headersHelperClass($this);
    }
}
