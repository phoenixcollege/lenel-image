<?php

namespace Smorken\LenelImage\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Smorken\LenelImage\Contracts\Services\ImageResult;
use Smorken\LenelImage\Contracts\Services\ImageService;

class Controller extends \Smorken\Controller\Controller
{
    public function __construct(
        protected ImageService $service
    ) {
        parent::__construct();
    }

    public function download(Request $request, int|string $studentId): Response
    {
        $result = $this->service->findByStudentId($request, $studentId);

        return $this->makeResponse($result, true);
    }

    public function downloadAlternateId(Request $request, int|string $alternateId): Response
    {
        $result = $this->service->findByAlternateId($request, $alternateId);

        return $this->makeResponse($result, true);
    }

    public function downloadStudentIdAlternateId(
        Request $request,
        int|string $studentId,
        int|string $alternateId
    ): Response {
        $result = $this->service->findByStudentIdAlternateId($request, $studentId, $alternateId);

        return $this->makeResponse($result, true);
    }

    public function view(Request $request, int|string $studentId, int $encode = 0): Response
    {
        $this->setEncode($encode);
        $result = $this->service->findByStudentId($request, $studentId);

        return $this->makeResponse($result, false);
    }

    public function viewAlternateId(Request $request, int|string $alternateId, int $encode = 0): Response
    {
        $this->setEncode($encode);
        $result = $this->service->findByAlternateId($request, $alternateId);

        return $this->makeResponse($result, false);
    }

    public function viewStudentIdAlternateId(
        Request $request,
        int|string $studentId,
        int|string $alternateId,
        int $encode = 0
    ): Response {
        $this->setEncode($encode);
        $result = $this->service->findByStudentIdAlternateId($request, $studentId, $alternateId);

        return $this->makeResponse($result, false);
    }

    protected function makeResponse(ImageResult $result, bool $download): Response
    {
        return $result->toResponse($download);
    }

    protected function setEncode(int $encode): void
    {
        if ($encode) {
            $this->service->setEncode(true);
        }
    }
}
