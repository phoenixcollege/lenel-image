<?php

namespace Smorken\LenelImage\Enums;

enum DisplayType
{
    case VIEW;
    case DOWNLOAD;
}
