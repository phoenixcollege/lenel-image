<?php

namespace Smorken\LenelImage\Models\Eloquent;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\LenelImage\Contracts\Enums\ImageIdKeys;
use Smorken\LenelImage\Contracts\Models\Image;
use Smorken\Model\Eloquent;

class Lenel extends Eloquent implements \Smorken\LenelImage\Contracts\Models\Lenel
{
    protected static array $idKeys = [
        ImageIdKeys::STUDENT_ID => 'SSONO',
        ImageIdKeys::ALTERNATE_ID => 'OPHONE',
    ];

    protected $connection = 'lenel_image';

    protected $table = 'MMOBJS';

    public static function setIdKeys(array $idKeys): void
    {
        foreach ($idKeys as $key => $value) {
            self::$idKeys[$key] = $value;
        }
    }

    public function findBy(int|string $studentId, int|string $alternateId): ?Image
    {
        if ($studentId || $alternateId) {
            $query = $this->getBaseLookupQuery();
            /** @var \Smorken\LenelImage\Contracts\Models\Lenel $model */
            $model = $query->where(function (Builder $query) use ($studentId, $alternateId) {
                if ($studentId) {
                    $this->scopeStudentIdIs($query, $studentId);
                }
                if ($alternateId) {
                    $this->scopeAlternateIdIs($query, $alternateId, (bool) $studentId);
                }
            })
                ->first();

            return $this->convertModelToImage($model);
        }

        return null;
    }

    public function findByAlternateId(int|string $alternateId): ?Image
    {
        $query = $this->getBaseLookupQuery();
        /** @var \Smorken\LenelImage\Contracts\Models\Lenel $model */
        $model = $this->scopeAlternateIdIs($query, $alternateId)->first();

        return $this->convertModelToImage($model);
    }

    public function findByStudentId(int|string $studentId): ?Image
    {
        $query = $this->getBaseLookupQuery();
        /** @var \Smorken\LenelImage\Contracts\Models\Lenel $model */
        $model = $this->scopeStudentIdIs($query, $studentId)->first();

        return $this->convertModelToImage($model);
    }

    public function scopeAlternateIdIs(Builder $query, int|string $alternateId, bool $or = false): Builder
    {
        if ($or) {
            return $query->orWhere($this->getIdKey(ImageIdKeys::ALTERNATE_ID), '=', $alternateId);
        }

        return $query->where($this->getIdKey(ImageIdKeys::ALTERNATE_ID), '=', $alternateId);
    }

    public function scopeStudentIdIs(Builder $query, int|string $studentId): Builder
    {
        return $query->where($this->getIdKey(ImageIdKeys::STUDENT_ID), '=', $studentId);
    }

    protected function convertModelToImage(?\Smorken\LenelImage\Contracts\Models\Lenel $lenel): ?Image
    {
        if ($lenel) {
            return new \Smorken\LenelImage\Models\Image('image/png', $lenel->LNL_BLOB, strlen($lenel->LNL_BLOB));
        }

        return null;
    }

    protected function getBaseLookupQuery(): Builder
    {
        /** @var Builder */
        return $this->newQuery()
            ->join('EMP as e', 'e.ID', '=', 'MMOBJS.EMPID')
            ->join('UDFEMP as u', 'u.ID', '=', 'e.ID')
            ->where('OBJECT', '=', 1)
            ->where('TYPE', '=', 0)
            ->orderBy('MMOBJS.LASTCHANGED', 'desc')
            ->orderBy('e.LASTCHANGED', 'desc');
    }

    /**
     * @phpstan-param  ImageIdKeys::*  $type
     */
    protected function getIdKey(string $type): string
    {
        return self::$idKeys[$type];
    }
}
