<?php

namespace Smorken\LenelImage\Models;

class Image implements \Smorken\LenelImage\Contracts\Models\Image
{
    public function __construct(
        public string $mime,
        public string $data,
        public int $size
    ) {}
}
