## Laravel 9 Lenel Image package

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Requirements
* PHP 8.0+
* PHP GD library
* [Composer](https://getcomposer.org/)


#### Use

Create a `lenel_image` connection in your `config/database.php`
