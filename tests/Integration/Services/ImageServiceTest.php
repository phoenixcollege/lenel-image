<?php

namespace Tests\Smorken\LenelImage\Integration\Services;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Http\Request;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Image\Sizer\Sizer;
use Smorken\LenelImage\Contracts\Services\ImageService;
use Smorken\LenelImage\Contracts\Storage\Lenel;
use Smorken\LenelImage\Models\Image;
use Tests\Smorken\LenelImage\Stubs\User;

class ImageServiceTest extends TestCase
{
    public function testFindByStudentIdAndAlternateIdEmptyAlternateIdWithResult(): void
    {
        $sut = $this->getSut();
        $sut->getProvider()->shouldReceive('findByStudentIdAlternateId')
            ->once()
            ->with(1234, '')
            ->andReturn($this->getImage('1x1.png'));
        $result = $sut->findByStudentIdAlternateId($this->getRequest(1), 1234, '');
        $result->getEventDispatcher()->shouldReceive('dispatch');
        $this->assertEquals(334, $result->result->size);
        $this->assertStringStartsWith('iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pV',
            base64_encode($result->result->data));
        $this->assertEquals('1234', $result->identifier);
        $this->assertEquals(1, $result->requesterId);
        $expected = [
            'Content-Type' => 'image/png',
            'Content-Transfer-Encoding' => 'binary',
            'Content-Disposition' => 'attachment; filename="photo-1234.png"',
            'Content-Length' => 334,
            'Cache-Control' => 'must-revalidate',
            'Expires' => '0',
            'Pragma' => 'public',
        ];
        $this->assertEquals($expected, $result->getHeaders(true));
    }

    public function testFindByStudentIdAndAlternateIdWithResult(): void
    {
        $sut = $this->getSut();
        $sut->getProvider()->shouldReceive('findByStudentIdAlternateId')
            ->once()
            ->with(1234, 'abcdef')
            ->andReturn($this->getImage('1x1.png'));
        $result = $sut->findByStudentIdAlternateId($this->getRequest(1), 1234, 'abcdef');
        $result->getEventDispatcher()->shouldReceive('dispatch');
        $this->assertEquals(334, $result->result->size);
        $this->assertStringStartsWith('iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pV',
            base64_encode($result->result->data));
        $this->assertEquals('1234:abcdef', $result->identifier);
        $this->assertEquals(1, $result->requesterId);
        $expected = [
            'Content-Type' => 'image/png',
            'Content-Transfer-Encoding' => 'binary',
            'Content-Disposition' => 'attachment; filename="photo-1234abcdef.png"',
            'Content-Length' => 334,
            'Cache-Control' => 'must-revalidate',
            'Expires' => '0',
            'Pragma' => 'public',
        ];
        $this->assertEquals($expected, $result->getHeaders(true));
    }

    public function testFindByStudentIdWithEncodedResult(): void
    {
        $sut = $this->getSut();
        $sut->setEncode(true);
        $sut->getProvider()->shouldReceive('findByStudentId')
            ->once()
            ->with(1234)
            ->andReturn($this->getImage('1x1.png'));
        $result = $sut->findByStudentId($this->getRequest(1), 1234);
        $result->getEventDispatcher()->shouldReceive('dispatch');
        $this->assertEquals(334, $result->result->size);
        $this->assertStringStartsWith('iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pV',
            $result->result->data);
        $this->assertEquals(1234, $result->identifier);
        $this->assertEquals(1, $result->requesterId);
    }

    public function testFindByStudentIdWithResult(): void
    {
        $sut = $this->getSut();
        $sut->getProvider()->shouldReceive('findByStudentId')
            ->once()
            ->with(1234)
            ->andReturn($this->getImage('1x1.png'));
        $result = $sut->findByStudentId($this->getRequest(1), 1234);
        $result->getEventDispatcher()->shouldReceive('dispatch');
        $this->assertEquals(334, $result->result->size);
        $this->assertStringStartsWith('iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pV',
            base64_encode($result->result->data));
        $this->assertEquals(1234, $result->identifier);
        $this->assertEquals(1, $result->requesterId);
        $expected = [
            'Content-Type' => 'image/png',
            'Content-Transfer-Encoding' => 'binary',
            'Content-Disposition' => 'attachment; filename="photo-1234.png"',
            'Content-Length' => 334,
            'Cache-Control' => 'must-revalidate',
            'Expires' => '0',
            'Pragma' => 'public',
        ];
        $this->assertEquals($expected, $result->getHeaders(true));
    }

    public function testFindByStudentIdWithoutResult(): void
    {
        $sut = $this->getSut();
        $sut->getProvider()->shouldReceive('findByStudentId')
            ->once()
            ->with(1234)
            ->andReturn(null);
        $result = $sut->findByStudentId($this->getRequest(1), 1234);
        $result->getEventDispatcher()->shouldReceive('dispatch');
        $this->assertNull($result->result);
        $this->assertEquals(1234, $result->identifier);
        $this->assertEquals(1, $result->requesterId);
    }

    public function testFindByStudentIdWithoutResultWithoutRequesterId(): void
    {
        $sut = $this->getSut();
        $sut->getProvider()->shouldReceive('findByStudentId')
            ->once()
            ->with(1234)
            ->andReturn(null);
        $result = $sut->findByStudentId($this->getRequest(0), 1234);
        $result->getEventDispatcher()->shouldReceive('dispatch');
        $this->assertNull($result->result);
        $this->assertEquals(1234, $result->identifier);
        $this->assertEquals(0, $result->requesterId);
    }

    protected function getImage(string $file, string $mime = 'image/png'): \Smorken\LenelImage\Contracts\Models\Image
    {
        $data = $this->getImageData($file);

        return new Image($mime, $data, strlen($data));
    }

    protected function getImageData(string $file): string
    {
        return file_get_contents(__DIR__.'/../../images/'.$file);
    }

    protected function getRequest(int $userId): Request
    {
        $request = new Request;
        $request->setUserResolver(function () use ($userId) {
            if ($userId) {
                return new User($userId);
            }

            return null;
        });

        return $request;
    }

    protected function getSut(): ImageService
    {
        return new \Smorken\LenelImage\Services\ImageService(
            m::mock(Lenel::class),
            new Sizer([
                'height' => 200,
                'width' => 200,
                'option' => 'auto',
                'output' => 'jpg',
                'base64' => false,
            ]),
            m::mock(Dispatcher::class),
            [
                'height' => 100,
                'width' => 100,
                'option' => 'auto',
                'output' => 'png',
                'base64' => false,
            ]
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
