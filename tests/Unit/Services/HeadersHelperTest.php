<?php

namespace Tests\Smorken\LenelImage\Unit\Services;

use PHPUnit\Framework\TestCase;
use Smorken\Image\Sizer\Result;
use Smorken\LenelImage\Contracts\Services\HeadersHelper;
use Smorken\LenelImage\Contracts\Services\ImageResult;

class HeadersHelperTest extends TestCase
{
    public function testForDownloadWithNullResult(): void
    {
        $sut = $this->getSut(new \Smorken\LenelImage\Services\ImageResult('123', 'abc'));
        $headers = $sut->getHeaders(true);
        $expected = [
            'Content-Type' => 'image/png',
            'Content-Transfer-Encoding' => 'binary',
            'Content-Disposition' => 'attachment; filename="photo-123.png"',
            'Content-Length' => 0,
            'Cache-Control' => 'must-revalidate',
            'Expires' => '0',
            'Pragma' => 'public',
        ];
        $this->assertEquals($expected, $headers);
    }

    public function testForDownloadWithResult(): void
    {
        $result = new \Smorken\LenelImage\Services\ImageResult('123', 'abc');
        $data = file_get_contents(__DIR__.'/../../images/1x1.png');
        $result->result = new Result([
            'mime' => 'image/png',
            'data' => $data,
            'size' => strlen($data),
        ]);
        $sut = $this->getSut($result);
        $headers = $sut->getHeaders(true);
        $expected = [
            'Content-Type' => 'image/png',
            'Content-Transfer-Encoding' => 'binary',
            'Content-Disposition' => 'attachment; filename="photo-123.png"',
            'Content-Length' => 150,
            'Cache-Control' => 'must-revalidate',
            'Expires' => '0',
            'Pragma' => 'public',
        ];
        $this->assertEquals($expected, $headers);
    }

    public function testForViewWithNullResult(): void
    {
        $sut = $this->getSut(new \Smorken\LenelImage\Services\ImageResult('123', 'abc'));
        $headers = $sut->getHeaders(false);
        $expected = [
            'Content-Type' => 'image/png',
            'Content-Transfer-Encoding' => 'binary',
            'Content-Disposition' => 'inline; filename="photo-123.png"',
            'Cache-Control' => 'max-age=86400, public',
            'Pragma' => 'public',
        ];
        $this->compareHeaders($expected, $headers, 6);
        $this->assertStringStartsWith(substr($sut->getExpires(), 0, 16), $headers['Expires']);
    }

    protected function compareHeaders(array $expected, array $headers, int $count): void
    {
        $this->assertCount($count, $headers);
        foreach ($expected as $key => $value) {
            $this->assertEquals($value, $headers[$key] ?? null, 'Header: '.$key);
        }
    }

    protected function getSut(ImageResult $imageResult): HeadersHelper
    {
        return new \Smorken\LenelImage\Services\HeadersHelper($imageResult);
    }
}
