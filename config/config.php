<?php

return [
    'model' => [
        'class' => \Smorken\LenelImage\Models\Eloquent\Lenel::class,
        'student_id_key' => env('LENEL_STUDENT_ID_KEY', 'SSNO'),
        'alternate_id_key' => env('LENEL_ALTERNATE_ID_KEY', 'OPHONE'),
    ],
    'storage' => [
        'class' => \Smorken\LenelImage\Storage\Eloquent\Lenel::class,
    ],
    'image' => [
        'height' => 200,
        'width' => 200,
        'option' => 'auto',
        'output' => 'png',
        'base64' => false,
    ],
];
